# Imagen base
FROM node:8

#Descripción
LABEL Author = "Jhon Cueva"

#Creación de variables de entorno
ENV NODEAPP /nodeapp

# Crear carpeta
RUN mkdir ${NODEAPP}

# Definir el directorio actual
WORKDIR ${NODEAPP}/

# Copiar
COPY package.json ${NODEAPP}

# Instalar dependencias
RUN npm install

# Copiar
COPY . ${NODEAPP}
# Expone el puerto 8000 del contenedor
EXPOSE 8080
# VOLUME [ "/nodeapp" ]
#Define el comando final que se ejecutará
CMD [ "npm", "start" ]